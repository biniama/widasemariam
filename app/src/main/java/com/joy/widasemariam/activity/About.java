package com.joy.widasemariam.activity;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.widget.TextView;

import com.joy.widasemariam.R;

public class About extends AbstractActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
	
		super.onCreate(savedInstanceState);

        setContentView(R.layout.about);

        final TextView aboutTheApp = (TextView) findViewById(R.id.about_the_app_label);

        // Get the preference set as default font size or use 20 font size
     	final SharedPreferences getPrefs = PreferenceManager.getDefaultSharedPreferences(getBaseContext());

     	String fontSize = getPrefs.getString("fontSizeListKey", "26");
     	
     	aboutTheApp.setTextSize(Float.parseFloat(fontSize) + 4.0F);
	}

}

package com.joy.widasemariam.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.joy.widasemariam.Constants;
import com.joy.widasemariam.R;
import com.joy.widasemariam.WithAboutMenu;
import com.joy.widasemariam.WithExitMenu;
import com.joy.widasemariam.WithHelpMenu;
import com.joy.widasemariam.WithPrefsMenu;
import com.joy.widasemariam.model.Language;

public class ChooseLanguage extends AbstractActivity implements WithAboutMenu, WithHelpMenu, WithPrefsMenu, WithExitMenu {
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		
		super.onCreate(savedInstanceState);
		
		final SharedPreferences getPrefs = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
		
		String defaultLanguageSelected = getPrefs.getString("languageListKey", Constants.ACTIVITY_PROPERTY_NO_LANGUAGE_SELECTED);
		
		// If default language is selected in the Preferences Menu
		if(defaultLanguageSelected != null && !defaultLanguageSelected.contentEquals(Constants.ACTIVITY_PROPERTY_NO_LANGUAGE_SELECTED)) {

			goToHome(Language.valueOf(defaultLanguageSelected));
			
		} else {

			setContentView(R.layout.choose_language);

			initializeView();
		}
	}

    private void initializeView() {
		
		final TextView chooseLanguageListLabel = (TextView) findViewById(R.id.choose_language_list_label);
		
		chooseLanguageListLabel.setText(R.string.choose_language_list_label);

		final Button chooseLanguageAmarignaButton = (Button) findViewById(R.id.choose_language_button_am);
		
		chooseLanguageAmarignaButton.setText(R.string.choose_language_button_am);
		
		chooseLanguageAmarignaButton.setOnClickListener(arg -> goToHome(Language.AMHARIGNA));
		
		
		final Button chooseLanguageGeezButton = (Button) findViewById(R.id.choose_language_button_ge);
		
		chooseLanguageGeezButton.setText(R.string.choose_language_button_ge);
		
		chooseLanguageGeezButton.setOnClickListener(arg -> goToHome(Language.GEEZ));

		
		final Button chooseLanguageEnglishButton = (Button) findViewById(R.id.choose_language_button_en);
		
		chooseLanguageEnglishButton.setText(R.string.choose_language_button_en);

		chooseLanguageEnglishButton.setOnClickListener(arg -> goToHome(Language.ENGLISH));


		final Button chooseLanguageDeutschButton = (Button) findViewById(R.id.choose_language_button_de);

		chooseLanguageDeutschButton.setText(R.string.choose_language_button_de);

		chooseLanguageDeutschButton.setOnClickListener(arg -> goToHome(Language.DEUTSCH));
	}

	protected void goToHome(Language selectedLanguage) {
		
		final Intent intent = new Intent(ChooseLanguage.this, Home.class);
		
		intent.putExtra(Constants.ACTIVITY_PROPERTY_SELECTED_LANGUAGE, selectedLanguage.name());
		
		startActivity(intent);
		
	}

	// pressing back on the choose language screen will close the app
//    @Override
//    public void onBackPressed() {
//		finish();
//    }

	// Source: https://stackoverflow.com/questions/8430805/clicking-the-back-button-twice-to-exit-an-activity
	private static final int TIME_INTERVAL = 2000; // # milliseconds, desired time passed between two back presses.
	private long mBackPressed;

	@Override
	public void onBackPressed() {
		if (mBackPressed + TIME_INTERVAL > System.currentTimeMillis()) {
			finishAffinity();
		} else {
			Toast.makeText(getBaseContext(), R.string.tap_back_button_twice_to_exit, Toast.LENGTH_SHORT).show();
		}

		mBackPressed = System.currentTimeMillis();
	}


}

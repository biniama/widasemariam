package com.joy.widasemariam.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

import com.joy.widasemariam.Constants;
import com.joy.widasemariam.R;
import com.joy.widasemariam.WithAboutMenu;
import com.joy.widasemariam.WithHelpMenu;
import com.joy.widasemariam.WithPrefsMenu;
import com.joy.widasemariam.model.Language;
import com.joy.widasemariam.model.PartOfBook;

public class Home extends AbstractActivity implements WithAboutMenu, WithHelpMenu, WithPrefsMenu {

    private String selectedLanguage;

    @Override
    protected void onSaveInstanceState(Bundle outState) {

        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.home);

        final Bundle extras = getIntent().getExtras();

        if (extras != null && extras.containsKey(Constants.ACTIVITY_PROPERTY_SELECTED_LANGUAGE)) {

            selectedLanguage = extras.getString(Constants.ACTIVITY_PROPERTY_SELECTED_LANGUAGE);
        }

        setLocale();

        initializeHomeView();
    }

    private void setLocale() {

        if (null != selectedLanguage) {

            if (selectedLanguage.equals(Language.AMHARIGNA.name())) {

                changeLocale(Constants.ACTIVITY_PROPERTY_LOCALE_AMHARIC);

            } else if (selectedLanguage.equals(Language.GEEZ.name())) {

                changeLocale(Constants.ACTIVITY_PROPERTY_LOCALE_GEEZ);

            } else if (selectedLanguage.equals(Language.ENGLISH.name())) {

                changeLocale(Constants.ACTIVITY_PROPERTY_LOCALE_ENGLISH);

            } else if (selectedLanguage.equals(Language.DEUTSCH.name())) {

                changeLocale(Constants.ACTIVITY_PROPERTY_LOCALE_DEUTSCH);
            }
        }
    }

    private void initializeHomeView() {

//		if(!isProcessingDialogShowing())
//			showProcessingDialog(getResources().getString(Integer.parseInt(Constants.MESSAGE_R_STRING_PREFIX + "general_loading_msg_" + selectedLanguage.toLowerCase().substring(0, 2))));

        final TextView chooseThePartYouWantToRead = (TextView) findViewById(R.id.home_part_of_book_list_label);
        chooseThePartYouWantToRead.setText(R.string.home_part_of_book_list_label);

        final Button choosePartZeweterButton = (Button) findViewById(R.id.home_day_zeweter_button);
        choosePartZeweterButton.setText(R.string.home_day_zeweter_button);
        choosePartZeweterButton.setOnClickListener(new OnClickListener() {

            public void onClick(View arg0) {

                goToReader(PartOfBook.ZEWETER);

            }
        });

        final Button choosePartMondayButton = (Button) findViewById(R.id.home_day_monday_button);
        choosePartMondayButton.setText(R.string.home_day_monday_button);
        choosePartMondayButton.setOnClickListener(new OnClickListener() {

            public void onClick(View arg0) {

                goToReader(PartOfBook.MONDAY);

            }
        });

        final Button choosePartTuesdayButton = (Button) findViewById(R.id.home_day_tuesday_button);
        choosePartTuesdayButton.setText(R.string.home_day_tuesday_button);
        choosePartTuesdayButton.setOnClickListener(new OnClickListener() {

            public void onClick(View arg0) {

                goToReader(PartOfBook.TUESDAY);

            }
        });

        final Button choosePartWednesdayButton = (Button) findViewById(R.id.home_day_wednesday_button);
        choosePartWednesdayButton.setText(R.string.home_day_wednesday_button);
        choosePartWednesdayButton.setOnClickListener(new OnClickListener() {

            public void onClick(View arg0) {

                goToReader(PartOfBook.WEDNESDAY);

            }
        });

        final Button choosePartThursdayButton = (Button) findViewById(R.id.home_day_thursday_button);
        choosePartThursdayButton.setText(R.string.home_day_thursday_button);
        choosePartThursdayButton.setOnClickListener(new OnClickListener() {

            public void onClick(View arg0) {

                goToReader(PartOfBook.THURSDAY);

            }
        });

        final Button choosePartFridayButton = (Button) findViewById(R.id.home_day_friday_button);
        choosePartFridayButton.setText(R.string.home_day_friday_button);
        choosePartFridayButton.setOnClickListener(new OnClickListener() {

            public void onClick(View arg0) {

                goToReader(PartOfBook.FRIDAY);

            }
        });

        final Button choosePartSaturdayButton = (Button) findViewById(R.id.home_day_saturday_button);
        choosePartSaturdayButton.setText(R.string.home_day_saturday_button);
        choosePartSaturdayButton.setOnClickListener(new OnClickListener() {

            public void onClick(View arg0) {

                goToReader(PartOfBook.SATURDAY);

            }
        });

        final Button choosePartSundayButton = (Button) findViewById(R.id.home_day_sunday_button);
        choosePartSundayButton.setText(R.string.home_day_sunday_button);
        choosePartSundayButton.setOnClickListener(new OnClickListener() {

            public void onClick(View arg0) {

                goToReader(PartOfBook.SUNDAY);

            }
        });

        // HIDING BUTTONS

        if (selectedLanguage.equals(Language.AMHARIGNA.name())) {

            findViewById(R.id.home_day_melka_mariam_button).setVisibility(View.GONE);

            findViewById(R.id.home_day_melka_eyesus_button).setVisibility(View.GONE);

            findViewById(R.id.home_day_melka_edom_button).setVisibility(View.GONE);
        }

        if (selectedLanguage.equals(Language.GEEZ.name())) {

            findViewById(R.id.home_day_melka_edom_button).setVisibility(View.GONE);
        }

        if (selectedLanguage.equals(Language.ENGLISH.name())) {

            findViewById(R.id.home_day_zeweter_button).setVisibility(View.GONE);

            findViewById(R.id.home_day_anqetse_birhan_button).setVisibility(View.GONE);

            findViewById(R.id.home_day_yiwedsewa_melaekt_button).setVisibility(View.GONE);

            findViewById(R.id.home_day_melka_mariam_button).setVisibility(View.GONE);

            findViewById(R.id.home_day_melka_eyesus_button).setVisibility(View.GONE);

            findViewById(R.id.home_day_melka_edom_button).setVisibility(View.GONE);
        }

        if (selectedLanguage.equals(Language.DEUTSCH.name())) {

            findViewById(R.id.home_day_melka_mariam_button).setVisibility(View.GONE);

            findViewById(R.id.home_day_melka_eyesus_button).setVisibility(View.GONE);

            findViewById(R.id.home_day_melka_edom_button).setVisibility(View.GONE);

        }

        final Button choosePartAnqetseBirhanButton = (Button) findViewById(R.id.home_day_anqetse_birhan_button);
        choosePartAnqetseBirhanButton.setText(R.string.home_day_anqetse_birhan_button);
        choosePartAnqetseBirhanButton.setOnClickListener(arg -> goToReader(PartOfBook.ANQETSE_BIRHAN));

        final Button choosePartYiwedsewaMelaektButton = (Button) findViewById(R.id.home_day_yiwedsewa_melaekt_button);
        choosePartYiwedsewaMelaektButton.setText(R.string.home_day_yiwedsewa_melaekt_button);
        choosePartYiwedsewaMelaektButton.setOnClickListener(arg -> goToReader(PartOfBook.YIWEDSEWA_MELAEKT));

        final Button choosePartMelkaMariamButton = (Button) findViewById(R.id.home_day_melka_mariam_button);
        choosePartMelkaMariamButton.setText(R.string.home_day_melka_mariam_button);
        choosePartMelkaMariamButton.setOnClickListener(arg -> goToReader(PartOfBook.MELKA_MARIAM));

        final Button choosePartMelkaEyesusButton = (Button) findViewById(R.id.home_day_melka_eyesus_button);
        choosePartMelkaEyesusButton.setText(R.string.home_day_melka_eyesus_button);
        choosePartMelkaEyesusButton.setOnClickListener(arg -> goToReader(PartOfBook.MELKA_EYESUS));

        final Button choosePartMelkaEdomButton = (Button) findViewById(R.id.home_day_melka_edom_button);
        choosePartMelkaEdomButton.setText(R.string.home_day_melka_edom_button);
        choosePartMelkaEdomButton.setOnClickListener(arg -> goToReader(PartOfBook.MELKA_EDOM));


        final Button goToChooseLanguageButton = (Button) findViewById(R.id.home_go_to_choose_language_button);
        goToChooseLanguageButton.setText(R.string.home_go_to_choose_language_button);
        goToChooseLanguageButton.setOnClickListener(arg -> goToChooseLanguage(Home.this));

        //todo no processing dialog before - can be removed
        hideProcessingDialog();
    }

    protected void goToReader(PartOfBook partOfBook) {

        final Intent intent = new Intent(Home.this, Reader.class);

        intent.putExtra(Constants.ACTIVITY_PROPERTY_SELECTED_LANGUAGE, selectedLanguage);

        intent.putExtra(Constants.ACTIVITY_PROPERTY_SELECTED_PART_OF_BOOK, partOfBook.name());

        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}